package solomia;

import solomia.model.Logic;
import solomia.model.WorkWithText;

public class Application {
    public static void main(String[] args) {
        WorkWithText work= new WorkWithText();
        Logic logic= new Logic();
        logic.countSentenceRepeatWord();
        logic.showSentenceByWordNumber();

    }
}

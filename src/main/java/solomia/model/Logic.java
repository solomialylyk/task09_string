package solomia.model;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import solomia.Application;

import java.util.Comparator;
import java.util.List;

public class Logic {
    private  Text text;
    private List<Sentence> sentences;
    private static Logger log= LogManager.getLogger(Application.class);

    public Logic() {
        WorkWithText workWithText= new WorkWithText();
        this.text = workWithText.createText();
        this.sentences = text.getSentencesList();
    }
    public void countSentenceRepeatWord() {
        int count;
        count = (int) sentences.stream()
                .filter(s -> s.getWordList().size() != s.getWordList()
                        .stream()
                        .distinct()
                        .count())
                .count();
        log.info(count);
    }

    public void showSentenceByWordNumber() {
        Comparator<Sentence> comparator = Comparator.comparing(s1 -> s1.getWordList().size());
        sentences.stream().sorted(comparator).forEach(sentence -> log.info(sentence.getSentence()));
    }

}

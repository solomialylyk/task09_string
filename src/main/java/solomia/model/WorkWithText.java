package solomia.model;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import solomia.Application;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class WorkWithText {
    private static Logger logger1= LogManager.getLogger(Application.class);
    public Text createText() {
        String mytext = "I love progamming. It is very fun. I do not think so.";
        Text text = new Text(mytext);
        text.setSentencesList(generateSentenceList(text.getText()));
        text.setWordsList(geterateWordList(text.getText()));
        text.getSentencesList().forEach(s -> s.setWordList(geterateWordList(s.getSentence())));
        return text;
    }

    private List<Word> geterateWordList(String text) {
        List<Word> wordList= new ArrayList<>();
        Pattern pattern = Pattern.compile("([^\\\\s,.!?]+)");
        Matcher matcher = pattern.matcher(text);
        while(matcher.find()){
            String str = matcher.group();
            wordList.add(new Word(str));
        }
        return  wordList;
    }

    private List<Sentence> generateSentenceList(String text) {
        List<Sentence> sentenceList = new ArrayList<>();
        Pattern pattern = Pattern.compile("[^.?!]+[.?!]");
        Matcher matcher = pattern.matcher(text);
        while(matcher.find()){
            String str = matcher.group();
            sentenceList.add(new Sentence(str));
        }
        return sentenceList;
    }


//    public static void main(String[] args) {
//        String mytext="I love progamming. It is very fun. I do not think so.";
//        Text text= new Text(mytext);
//    }

}

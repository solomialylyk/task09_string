package solomia.model;

import java.util.List;

public class Text {

    private String text;
    private List<Sentence> sentencesList;
    private List<Word> wordsList;

    public Text(String text) {
        this.text = text;
    }

    public String getText() {
        return text;
    }

    public List<Sentence> getSentencesList() {
        return sentencesList;
    }

    public void setSentencesList(List<Sentence> sentencesList) {
        this.sentencesList = sentencesList;
    }

    public List<Word> getWordsList() {
        return wordsList;
    }

    public void setWordsList(List<Word> wordsList) {
        this.wordsList = wordsList;
    }
}